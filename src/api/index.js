import Vue from 'vue'

class Api {
  constructor () {
    this.base = 'http://orgapi.dev'
    this.match = {
      'login': {
        url: '/user/login',
        method: 'post'
      },
      'spacesList': {
        url: '/spaces',
        method: 'get'
      },
      'spaceDetail': {
        url: '/spaces/:spacename',
        method: 'get'
      },
      'moduleDetail': {
        url: ':spaceid/modules/:modulename',
        method: 'get'
      }
    }
  }
  execute (index, successCb, failCb, datas = {}, params = {}) {
    var match = this.match[index]
    var url = this.getUrl(match)
    switch (match.method) {
      case 'get':
        // remplace les params dans l'url
        if (Object.keys(params).length !== 0) {
          for (var p in params) {
            url = url.replace(':' + p, params[p])
          }
        }
        // ajoute des données dans l'url
        if (Object.keys(datas).length !== 0) {
          var gets = ''
          for (var d in datas) {
            if (gets !== '') {
              gets += '&'
            }
            gets += d + '=' + datas[d]
          }
          url += '?' + gets
        }
        // url += '?api_token=' + datas.token
        this.get(url, successCb, failCb)
        break
      case 'post':
        if (Object.keys(params).length !== 0) {
          for (var pa in params) {
            url = url.replace(':' + pa, params[pa])
          }
        }
        this.post(url, datas, successCb, failCb)
        break
      default:
        break
    }
  }
  getUrl (match) {
    return this.base + match.url
  }
  get (url, successCb, failCb) {
    Vue.http.get(url, { emulateJSON: true }).then(successCb, failCb)
  }
  post (url, datas, successCb, failCb) {
    Vue.http.post(url, datas, { emulateJSON: true }).then(successCb, failCb)
  }
}

export default Api
