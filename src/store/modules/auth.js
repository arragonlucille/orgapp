import Api from '../../api'
import router from '../../router'
import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT_ACTION } from '../mutation-types'
// initial state
const state = {
  apiToken: JSON.parse(localStorage.getItem('authToken')) || '',
  messageError: ''
}

const getters = {
  isLogged: (state, getters) => {
    return state.apiToken !== ''
  },
  isError: (state) => {
    return state.messageError !== ''
  }
}

const actions = {
  login: (context, datas) => {
    var a = new Api()
    context.commit(LOGIN_REQUEST)
    a.execute('login',
      (response) => { context.commit(LOGIN_SUCCESS, response.body) },
      (response) => { context.commit(LOGIN_FAIL, response.body) }, datas)
  },
  logout: (context) => {
    context.commit(LOGOUT_ACTION)
  }
}

const mutations = {
  [LOGOUT_ACTION]: () => {
    state.apiToken = ''
    localStorage.clear()
    router.push({ name: 'guestHome' })
  },
  [LOGIN_REQUEST] (state) {
    state.apiToken = ''
  },
  [LOGIN_SUCCESS] (state, datas) {
    state.apiToken = datas.datas
    localStorage.setItem('authToken', JSON.stringify(state.apiToken))
    router.push({ name: 'userHome' })
  },
  [LOGIN_FAIL] (state, datas) {
    state.apiToken = ''
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
