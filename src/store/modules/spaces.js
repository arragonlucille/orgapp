import Api from '../../api'
import { SPACES_INDEX_REQUEST, SPACES_INDEX_SUCCESS, SPACES_INDEX_FAIL, SPACE_DETAIL_REQUEST, SPACE_DETAIL_SUCCESS, SPACE_DETAIL_FAIL } from '../mutation-types'
// // initial state
const state = {
  spaces: [],
  messageError: '',
  space: {}
}

const getters = {
  getSpaces: (state) => {
    return state.spaces
  },
  getSpace: (state) => {
    return state.space
  },
  getSpaceModule: (state) => {
    return state.space.modules
  },
  isError: (state) => {
    return state.messageError !== ''
  }
}

const actions = {
  getSpacesIndex: (context) => {
    var a = new Api()
    context.commit(SPACES_INDEX_REQUEST)
    a.execute('spacesList',
      (response) => { context.commit(SPACES_INDEX_SUCCESS, response.body) },
      (response) => { context.commit(SPACES_INDEX_FAIL, response.body) },
      { api_token: context.rootState.auth.apiToken }
    )
  },
  getDatasToSpace: (context, spacename) => {
    // console.log(context.state.spaces, this.state)
    // var spaces = context.state.spaces
    // console.log(truc)
    var a = new Api()
    context.commit(SPACE_DETAIL_REQUEST)
    a.execute('spaceDetail',
      (response) => { context.commit(SPACE_DETAIL_SUCCESS, response.body) },
      (response) => { context.commit(SPACE_DETAIL_FAIL, response.body) },
      { api_token: context.rootState.auth.apiToken }, { spacename: spacename }
    )
  }
}

const mutations = {
  [SPACES_INDEX_REQUEST] (state) {
    state.spaces = []
  },
  [SPACES_INDEX_SUCCESS] (state, response) {
    state.spaces = response.datas
  },
  [SPACES_INDEX_FAIL] (state, datas) {
    state.spaces = []
  },
  [SPACE_DETAIL_REQUEST] (state) {
    state.space = {}
  },
  [SPACE_DETAIL_SUCCESS] (state, response) {
    state.space = response.datas
  },
  [SPACE_DETAIL_FAIL] (state, datas) {
    state.space = {}
  }
}

export default {
  state,
  getters,
  actions,
  mutations,
  namespaced: true
}
