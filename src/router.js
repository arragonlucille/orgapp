import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './store'

Vue.use(VueRouter)

function load (component) {
  return () => System.import(`components/${component}.vue`)
}

export default new VueRouter({
  /*
   * NOTE! VueRouter "history" mode DOESN'T works for Cordova builds,
   * it is only to be used only for websites.
   *
   * If you decide to go with "history" mode, please also open /config/index.js
   * and set "build.publicPath" to something other than an empty string.
   * Example: '/' instead of current ''
   *
   * If switching back to default "hash" mode, don't forget to set the
   * build publicPath back to '' so Cordova builds work again.
   */

  routes: [
    { path: '/home',
      component: load('layouts/Guests'),
      name: 'guestHome',
      children: [
        { path: 'login', name: 'login', component: load('views/Login') }, // Login
        { path: 'register', name: 'register', component: load('views/Register') } // Register
      ],
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogged) {
          next({name: 'userHome'})
        }
        else {
          next()
        }
      }
    }, // Default Guest
    { path: '/',
      name: 'userHome',
      component: load('layouts/Users'),
      children: [
        { path: 'logout',
          name: 'logout',
          beforeEnter (to, from, next) {
            store.dispatch('logout')
            next({name: 'guestHome'})
          }
        },
        { path: ':spacename',
          component: load('layouts/SpaceUser'),
          props: true,
          children: [
            { path: '', name: 'spaceDashboard', component: load('views/SpaceDashboard'), props: true },
            { path: ':modulename', name: 'moduleView', component: load('views/ModuleDisplay'), props: true }
          ]
        }
      ],
      beforeEnter: (to, from, next) => {
        // TODO::Verify api_token
        // Verify if is logged
        if (store.getters.isLogged) {
          next()
        }
        else {
          next({name: 'guestHome'})
        }
      }
    }, // User connected
    { path: '*', component: load('Error404') } // Not found
  ]
})
